

module Data.Derive.Superclass
    (
    -- deriving_superclasses  
    -- , strategy_deriving_superclasses
    -- , newtype_deriving_superclasses
    -- , gnds
) where

import Data.Derive.TopDown.Lib
import Data.Derive.TopDown.IsInstance
import Language.Haskell.TH
import Language.Haskell.TH.Lib
import Debug.Trace
import Control.Monad
import Data.List
import Control.Monad.Trans.State
import Control.Monad.Trans
import Data.Maybe
import Language.Haskell.TH.Ppr

