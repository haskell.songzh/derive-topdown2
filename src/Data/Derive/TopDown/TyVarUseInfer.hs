{-# LANGUAGE MultiWayIf #-}

module Data.Derive.TopDown.TyVarUseInfer where

-- This module has function to infer the position of a type variable
-- This is like the role in Haskell but not the same

import Control.Monad
import Control.Monad.State
import Control.Monad.State.Class
import Control.Monad.Trans
import qualified Data.Set as S
-- import qualified Data.Map as M
import Data.Set (Set)
import Data.Map (Map)
import Data.Derive.TopDown.Lib
import qualified Data.Derive.TopDown.MultiMap as MM
import Language.Haskell.TH



{-| 3 cases: not used, nominal in type family, and representational  
  in data constructor fields 

  1. data Proxy a = Proxy
  2. a) data Poly k a = Leaf | Branch (k a) -- k and a are Nominal
     b) type family F a 
        data P a = Con (F a) -- a is Nominal
  3. data Maybe a = Nothing | Just a -- a is Rep

 use https://hackage.haskell.org/package/th-abstraction-0.4.5.0/docs/
 to handle GADTs
-}
data TyVarUse = Infer | Phm | Nom | Rep 
    deriving (Show, Eq, Ord, Enum)

prod :: [TyVarUse] -> TyVarUse
prod xs = maximum (Phm: xs)

data Env = Env {
--   tvbNames :: Set Name   -- ^ Type variable names
   getVarUse   :: MM.MultiMap Name TyVarUse
 , getTyInfer  :: Set Name -- ^ for handling recursive defined types
} deriving (Show, Eq)

lookupVarUse :: Name -> Env -> S.Set TyVarUse
lookupVarUse n e = MM.lookup n (getVarUse e)

insertVarUse :: Name -> TyVarUse -> Env -> Env
insertVarUse n v e = e{ getVarUse = MM.insert n v (getVarUse e) }

isTyRecursive :: Name -> VUIM Bool
isTyRecursive n = do 
          env <- get
          let s = getTyInfer env
          return $ n `S.member` s

type VarUseInferMonad a = StateT Env Q a

type VUIM a = VarUseInferMonad a

inferUse :: Name -> VUIM [TyVarUse]
inferUse n = do 
    (tvbs, cons) <- lift $ getTyVarCons n
    let tvns = map getTVBName tvbs
    inferUseTvsCons tvns cons
    
type TypeVarName = Name

inferUseTvsCons :: [TypeVarName] -> [Con] -> VUIM [TyVarUse]
inferUseTvsCons tvns cons = 
  forM tvns $ \tvn -> do
    inferUseTvCons tvn cons

inferUseTvCons :: TypeVarName -> [Con] -> VUIM TyVarUse
inferUseTvCons tvn cons = do
  uses <- forM cons $ \con -> do
            inferUseTvCon tvn con
  return $ prod uses

inferUseTvCon :: TypeVarName -> Con -> VUIM TyVarUse
inferUseTvCon tvn (NormalC _ bts) = do
   let tys = map snd bts
   inferUseTvInFields tvn tys
   return Phm

inferUseTvCon tvn (GadtC name bts t) = undefined

type Field = Type

inferUseTvInFields :: TypeVarName -> [Field] -> VUIM TyVarUse
inferUseTvInFields n fs = do 
   uses <- forM fs $ \f -> inferUseTvInField n f
   return $ prod uses


inferUseTvInField :: TypeVarName -> Field -> VUIM TyVarUse
inferUseTvInField n (ForallT tvbs c t) = undefined
inferUseTvInField n (ForallVisT tvb t) = undefined
inferUseTvInField n a@(AppT t1 t2) = do 
                    let lmt = getLeftMostType a
                    case lmt of
                      ConT cn -> do 
                        isTF <- lift $ isTypeFamily cn
                        isDN <- lift $ isDataNewtype cn
                        if | isTF -> if | (n `elem` getAllVarNames a) -> return Nom
                                        | otherwise -> return Phm
                           | isDN -> 
                             -- 1. lookup in Env, whether it's already in getTyInfer. if so return Phm
                             -- data Stream a = Stream (Stream a)   a is phatom
                             -- data List a = Nil | Cons a (List a) , in Cons, first 'a' is Rep, the 'a' in 
                             -- List a is phantom
                             -- 2. put recursive defined type into the Env
                             do 
                              isRec <- isTyRecursive cn
                              if isRec
                                then return Phm
                                else do
                                  (tvbs, cons) <- lift $ getTyVarCons cn
                                  return undefined
                      VarT vn -> return Nom

                    -- isTF <- isLeftMostAppTTypeVar a
                    -- if isTF
                    --   then if n `elem` getAllVarNames a
                    --     then do
                    --         env <- get                         
                    --         put $ insertVarUse n Rep env
                    --     else return ()
                    --   else do
                    --     isDN <- isLeftMostAppTDataNewtype
                    --     if isDN
                    --       then case getLeftMostType a of
                    --               ConT a -> 

inferUseTvInField n (AppKindT t _) = inferUseTvInField n t
inferUseTvInField n (SigT t _)     = inferUseTvInField n t
inferUseTvInField n (VarT vn) = if n == vn
                               then return Rep                                 
                               else return Phm
inferUseTvInField n (ConT cn) = undefined
inferUseTvInField n (PromotedT pn) = undefined

{-
data T a = X (Either (Maybe (Proxy a)) (Maybe a))

inferUseTvInTy a (Either (Maybe (Proxy a)) (Maybe a)) 
-}

-- Field与data type的参数的推断是分开的
-- Type is data or newtpe decl types
inferUseTvInTy :: TypeVarName -> Type -> VUIM TyVarUse
inferUseTvInTy n t@(AppT t1 t2) = do
                      let (c:cs) = unappTy t
                      uses <- inferUse n
                      let useAndType = filter (\(u, a) -> a /= Phm) (zip uses cs)                      
                      return undefined

inferUseTvInTy n _ = return Phm
