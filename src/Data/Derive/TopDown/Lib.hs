{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes#-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC -Wno-unused-imports #-}

module Data.Derive.TopDown.Lib where 

import Language.Haskell.TH
import Language.Haskell.TH.Syntax hiding (lift)
import Data.Generics
import GHC.Exts
import Language.Haskell.TH.ExpandSyns (expandSynsWith,noWarnTypeFamilies)
import Data.List (nub,intersect,foldr1)
import Control.Monad.State
import Control.Monad.Trans
import Control.Applicative
import Control.Monad

type ClassName = Name
type TypeName = Name

noWarnExpandSynsWith :: Type -> Q Type
noWarnExpandSynsWith = expandSynsWith noWarnTypeFamilies

{-|
  Get the type variable name.
-}
getVarName :: Type -> [Name]
getVarName (VarT n) = [n]
getVarName _ = []

{-|
  Get the type variable name.
-}
getAllVarNames :: Data a => a -> [Name]
getAllVarNames = everything (++) (mkQ [] getVarName)

{-|
  Is the type a type family
-}
isTypeFamily :: TypeName -> Q Bool
isTypeFamily tn = do
                info <- reify tn
                case info of
                  FamilyI (OpenTypeFamilyD _) _     -> return True
                  FamilyI (ClosedTypeFamilyD _ _) _ -> return True 
                  _                                 -> return False

isDataNewtype :: TypeName -> Q Bool
isDataNewtype tn = do
                info <- reify tn
                case info of
                  TyConI (DataD _ _ _ _ _ _)    -> return True
                  TyConI (NewtypeD _ _ _ _ _ _) -> return True 
                  _                             -> return False

{-
  For type appications like @(k a b)@, @Either Int a@, we always need to 
  get the left most type in such cases
-}
getLeftMostType :: Type -> Type
getLeftMostType (AppT t1 t2) = getLeftMostType t1
getLeftMostType t            = t

isLeftMostAppTTypeFamily :: Type -> Q Bool
isLeftMostAppTTypeFamily (getLeftMostType -> ConT n) = isTypeFamily n
isLeftMostAppTTypeFamily _                           = return False

isLeftMostAppTDataNewtype :: Type -> Q Bool
isLeftMostAppTDataNewtype (getLeftMostType -> ConT n) = isDataNewtype n
isLeftMostAppTDataNewtype _                           = return False

isLeftMostAppTTypeVar :: Type -> Q Bool
isLeftMostAppTTypeVar (getLeftMostType -> VarT n) = return True
isLeftMostAppTTypeVar _                           = return False

{-| 
  Get type variable name
-}
getTVBName :: TyVarBndr () -> Name
getTVBName (PlainTV name _)    = name
getTVBName (KindedTV name _ _) = name

{-| After unapplying left most cannot be AppT and AppKindT, but can be InfixT or others -}
unappTy :: Type -> [Type]
unappTy (AppT t1 t2) = unappTy t1 ++ [t2]
unappTy (AppKindT ty kind) = unappTy ty
unappTy t = [t]

getConstrArgs = tail . unappTy

#if __GLASGOW_HASKELL__ > 810
voidTyVarBndrFlag :: TyVarBndr flag -> TyVarBndr ()
voidTyVarBndrFlag (PlainTV n _) = PlainTV n ()
voidTyVarBndrFlag (KindedTV n _ k) = KindedTV n () k
#endif

isHigherOrderClass :: Name -> Q Bool
isHigherOrderClass ty = do
    cla <- reify ty; 
    case cla of
        ClassI (ClassD _ _ vars _ _) _ 
            -> do
              let (KindedTV _ _ k) = head vars
              if k == StarT
                  then return True
                  else return False; 
        _ -> error $ show ty ++ " is not a class";

{-| data T a1 a2 = Con1 a1 | Con2 a2 ...
 return [a1, a2], [Con1 a1, Con2 a2]
-}
getTyVarCons :: TypeName -> Q ([TyVarBndr ()], [Con])
getTyVarCons name = do
            info <- reify name
            case info of
              TyConI dec -> 
                case dec of
                  DataD    _ _ tvbs _ cons _  -> return (map voidTyVarBndrFlag tvbs, cons)
                  NewtypeD _ _ tvbs _ con  _  -> return (map voidTyVarBndrFlag tvbs, [con])
                  TySynD   name tvbs t        -> error $ show name ++ " is a type synonym and `TypeSynonymInstances' is not supported.\n"
                      ++ "If you did not derive it then this is a bug, please report this bug to the author of `derive-topdown' package."
                  x -> do
                      error $ pprint (x :: Dec) ++ " is not a data or newtype definition."
              _ -> error $ "Cannot generate instances for " ++ show name