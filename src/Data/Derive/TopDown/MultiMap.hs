module Data.Derive.TopDown.MultiMap
(
    MultiMap(..),
    empty,
    insert,
    lookup,
    singleton
) where

import Prelude hiding (lookup)
import qualified Data.Map as M
import Data.Map (Map)
import qualified Data.Set as S
import Data.Set (Set)

type MultiMap k v = Map k (Set v)

empty :: MultiMap k v
empty = M.empty

singleton :: k -> v -> MultiMap k v
singleton k a = M.singleton k (S.singleton a)

insert :: (Ord k, Ord v) => k -> v -> MultiMap k v -> MultiMap k v
insert k v m = case M.lookup k m of
                    Nothing -> M.insert k (S.singleton v) m
                    Just a  -> M.insert k (S.insert v a)  m

lookup :: Ord k => k -> MultiMap k v -> Set v
lookup k m = case M.lookup k m of 
                Nothing -> S.empty
                Just s  -> s

