
{-# LANGUAGE PartialTypeSignatures #-}

module Data.Derive.TopDown.CxtGen 
(
      genHoleCxtTy
    , genCxtTy
)
where

{-
-- This module contains functions with type ClassName -> TypeName -> Type

    There are 2 ways 
    1. deriving by making the context with all wholes with @PartialTypeSignatures@

    ```
    deriving instance _ => Eq (A a)

    ```
    
    2. deriving by generate the class context which can handle type family
-}

import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import Data.Derive.TopDown.Lib

{-| Get wildcard context
-}
genHoleCxtTy :: ClassName -> TypeName -> Q Type
genHoleCxtTy cn tn = return WildCardT

genCxtTy  :: ClassName -> TypeName -> Type
genCxtTy = undefined

data A a = A a

foo :: Q [Dec]
foo = [d|deriving instance _ => Ord (A a)|]

{-
  This module should generate 
-}